///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   01_26_21
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdbool.h>
#include "animals.h"


/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {

   // @todo Map the enum Color to a string
   switch(color){
      case 0:
         return "Black";
      case 1:
         return "White";
      case 2:
         return "Red";
      case 3:
         return "Blue";
      case 4:
         return "Green";
      case 5:
         return "Pink";
      default: return NULL;
   }
/*
   if(color == BLACK)
      return "Black";
   else if(color == WHITE)
      return "White";
   else if(color == RED)
       return "Red";
   else if(color == BLUE)
      return "Blue";
   else if(color == GREEN)
      return "Green";
   else if(color == PINK)
         return "Pink";
   else
      return NULL;
*/
   return NULL; // We should never get here
}

char* genderName (enum Gender gender){
   switch(gender){
      case 0:
         return "Male";
      case 1: 
         return "Female";
      default: return NULL;
     }  
/*   
   if(gender == MALE)
      return "Male";
   else if(gender == FEMALE)
      return "Female";
   else
      return NULL;
*/
   return NULL; // We should never get here
}

char* breedName (enum CatBreeds breed){
   switch(breed){
      case 0:
         return "Main Coon";
      case 1: 
         return "Manx";
      case 2: 
         return "Shorthair";
      case 3: 
         return "Persian";
      case 4: 
         return "Sphynx";
      default: return NULL;
   }
/*
   if(breed == MAIN_COON)
      return "Main Coon";
   else if(breed == MANX)
      return "Manx";
   else if(breed == SHORTHAIR)
      return "Shorthair";
   else if(breed == PERSIAN)
      return "Persian";
   else if(breed == SPHYNX)
      return "Sphynx";
   else
      return NULL;
*/
   return NULL; // We should never get here
}

char* isfixed (bool fixed){
   if(fixed == 1)
      return "Yes";
   else
      return "No";
}
